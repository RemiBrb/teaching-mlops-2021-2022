from locust import HttpUser, between, task


class APIUser(HttpUser):
    wait_time = between(1, 3)

    @task
    def intent_inference(self) -> None:
        sentence = "Je veux aller chez le fleuriste."
        self.client.get("/api/intent?sentence=" + sentence)

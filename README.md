# MLOPS
Authors : Rémi BARBOSA & Alban PERRIER

Context : AI software engineering - Bordeaux INP 2022

Subject : https://tinyurl.com/mlops-paper

# The project
Available on this URL : https://teaching-mlops-api.herokuapp.com/
The main code is located in the main.py file

## Tests
Unit and integration tests are located in the test_main.py file

## Benchmark
### Services
Our docker image weights 411.19 MB and uses around 717 MB of RAM ([root image](https://gitlab.com/RemiBrb/teaching-mlops-2021-2022/container_registry/2626682)). We could set the RAM environnement limit around 1024 MB for our needs.
![docker](images/docker.PNG)



### Stresstests
We tested the model with Locust, which gave us this results :
![locust](images/locust.PNG)
We can see that we have a P99 < 200ms for a traffic around 340 requests per second.

### Performances
The goal is to extend the data from the stack data (large dataset) with our model. Our current implementation presents an obvious limitation : it has only one working computer to deal sequentially with all the requests.

To solve this issue, we could try to deal those requests by batch. Indeed, we could divide the computable inputs by our model by batch so that it deals with them one by one. But still, it would require more memory in order to store the inputs waiting to be computed. An easy, but costly, solution would be to use several computers to increase the computing capacity.

import main
from main import app
from fastapi.testclient import TestClient


client = TestClient(app)


class TestMain:

    @staticmethod
    def test_intent_inference():
        if type(main.intent_inference("test sentence")) is dict:
            return True
        else:
            return False

    @staticmethod
    def test_supported_languages():
        dict = main.supported_languages()
        for value in dict.values():
            if value == "fr-FR":
                return True
        return False

    @staticmethod
    def test_health_check_endpoint():
        response = client.get("/health")
        assert response.status_code == 200
